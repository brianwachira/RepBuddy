import '@react-navigation/native';
import 'formik';

import oldProcess = require('process')
declare module '@react-navigation/native' {
  export type ExtendedTheme = {
    dark: boolean;
    colors: {
      bg: string;
      background: string;
      primary: string;
      secondary: string;
      primaryDarker: string;
      secondaryDarker: string;
      text: string;
      textPrimary: string;
      textSecondary: string;
      textSecondary2: string;
      textSecondary3: string;
      textSecondary4: string;
      textTertiary: string;
      gray3: string;
      disabled: string,
      success: string;
      tertiary: string;
      textWhite: string;
      black: string;
      primaryLight: string;
      whiteSecondary: string;
      progressInactive: string,
      placeholder: string
    };
  };

  export type ThemeColors = {
    [key in 'light' | 'dark']: Omit<ExtendedTheme['colors'], 'text' | 'border' | 'notification'>;
  };
  export function useTheme(): ExtendedTheme;
}

type Poppins = {
  300: string,
  400: string,
  500: string,
  700: string,
  600: string,
}

export type Fonts = {
  poppins: Poppins;
}

export type Typography = {
  primary: Poppins
}

export interface ISvgProps extends SvgProps {
  color?: string,
  xmlns?: string;
  xmlnsXlink?: string;
  xmlSpace?: string;
}


export interface IActivity {
  activity_name: string,
  activity_type: string,
  ambassador_id: string,
  ambassador_name: string,
  created_at: string,
  id: string,
  latitude: string,
  location: string,
  longitude: string,
  notes: string | null,
  pinned_location: string,
  product_id: string,
  sales_rep_id: string,
  sales_rep_name: string,
  scheduled_date: Date,
  status: string,
}