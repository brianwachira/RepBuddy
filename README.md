# RepBuddy

# Tech Stack

- Expo - [https://docs.expo.dev/](https://docs.expo.dev/)
- Typescript - [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- Expo Router - [https://docs.expo.dev/routing/introduction/](https://docs.expo.dev/routing/introduction/)
- Tanstack Query - [https://tanstack.com/query/v4/docs/react/react-native](https://tanstack.com/query/v4/docs/react/react-native)
