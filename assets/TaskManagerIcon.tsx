import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { ISvgProps } from "../global"

const TaskManagerIcon = (props: ISvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={26}
    height={24}
    fill="none"
    {...props}
  >
    <Path
      fill={props.color}
      d="M15.16 23.64c-.84.24-1.8.36-2.76.36-6.6 0-12-5.4-12-12s5.4-12 12-12c1.56 0 3.12.36 4.56.84l-1.92 1.92c-.84-.24-1.68-.36-2.64-.36-5.28 0-9.6 4.32-9.6 9.6 0 5.28 4.32 9.6 9.6 9.6.48 0 1.08 0 1.56-.12.24.84.72 1.56 1.2 2.16ZM7.48 9.72 5.8 11.4l5.4 5.4 12-12-1.68-1.68L11.2 13.44 7.48 9.72ZM19.6 14.4V18H16v2.4h3.6V24H22v-3.6h3.6V18H22v-3.6h-2.4Z"
    />
  </Svg>
)
export default TaskManagerIcon
