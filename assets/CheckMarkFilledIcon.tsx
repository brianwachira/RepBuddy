import * as React from "react";
import Svg, { Path } from "react-native-svg";
import { ISvgProps } from "../global";
const CheckMarkFilledIcon = (props: ISvgProps) => (
	<Svg
		xmlns="http://www.w3.org/2000/svg"
		width={22}
		height={23}
		fill="none"
		{...props}
	>
		<Path
			fill="#32936F"
			d="M11 1.875a9.625 9.625 0 1 0 0 19.25 9.625 9.625 0 0 0 0-19.25ZM9.625 15.343l-3.438-3.437 1.094-1.094 2.344 2.345 5.094-5.095 1.098 1.09-6.192 6.191Z"
		/>
	</Svg>
);
export default CheckMarkFilledIcon;
