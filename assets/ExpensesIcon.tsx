import * as React from "react"
import Svg, { SvgProps, G, Path, Defs, ClipPath } from "react-native-svg"
import { ISvgProps } from "../global"

const ExpensesIcon = (props: ISvgProps) => (
    <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={32}
    height={24}
    fill="none"
    {...props}
  >
    <G clipPath="url(#a)">
      <Path
        fill={props.color}
        d="M10 10a4 4 0 1 1 8 0 4 4 0 0 1-8 0Zm4-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4ZM3 0a3 3 0 0 0-3 3v14a3 3 0 0 0 3 3h22a3 3 0 0 0 3-3V3a3 3 0 0 0-3-3H3ZM2 3a1 1 0 0 1 1-1h3v2a2 2 0 0 1-2 2H2V3Zm0 5h2a4 4 0 0 0 4-4V2h12v2a4 4 0 0 0 4 4h2v4h-2a4 4 0 0 0-4 4v2H8v-2a4 4 0 0 0-4-4H2V8Zm20-6h3a1 1 0 0 1 1 1v3h-2a2 2 0 0 1-2-2V2Zm4 12v3a1 1 0 0 1-1 1h-3v-2a2 2 0 0 1 2-2h2ZM6 18H3a1 1 0 0 1-1-1v-3h2a2 2 0 0 1 2 2v2Zm24-1a5 5 0 0 1-5 5H4.17A3 3 0 0 0 7 24h18a7 7 0 0 0 7-7V7a3 3 0 0 0-2-2.83V17Z"
      />
    </G>
    <Defs>
      <ClipPath id="a">
        <Path fill="#fff" d="M0 0h32v24H0z" />
      </ClipPath>
    </Defs>
  </Svg>
)
export default ExpensesIcon
