import * as React from "react"
import Svg, { SvgProps, Path } from "react-native-svg"
import { ISvgProps } from "../global"


const RoutesIcon = (props: ISvgProps) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    fill="none"
    {...props}
  >
    <Path
      fill={props.color}
      d="M3.482 2.465A2.98 2.98 0 0 0 .499 5.448c0 2.237 2.983 5.539 2.983 5.539s2.983-3.302 2.983-5.539a2.98 2.98 0 0 0-2.983-2.983Zm0 4.048a1.065 1.065 0 1 1 0-2.13 1.065 1.065 0 0 1 0 2.13ZM18.514 9.27V4.963a4.97 4.97 0 0 0-.205-1.335 5.004 5.004 0 0 0-9.811 1.218v.249l-.01.871v9.968h-.004v.04a2.003 2.003 0 0 1-4.007 0c0-.011.003-.023.004-.036h-.004v-1.946H2.474v2.003h.002a4.004 4.004 0 1 0 8.009-.005h.002v-.984h.005V6.968l.01-1.873v-.248a3.006 3.006 0 0 1 6.01.123v4.282a2.002 2.002 0 1 0 2.002.018Z"
    />
  </Svg>
)
export default RoutesIcon
