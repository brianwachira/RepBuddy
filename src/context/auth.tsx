import {
	SplashScreen,
	useRootNavigation,
	useRouter,
	useSegments,
} from "expo-router";
import { useAsyncStorage } from "@react-native-async-storage/async-storage";
import { createContext, useContext, useEffect, useState } from "react";
import React from "react";

export type StagedUserType = {
	status: string;
	status_message: string;
	otp: string;
	data: Omit<UserType, "rate">;
	time: string;
	rate: string;
};

export type UserType = {
	id: string;
	names: string;
	msisdn: string;
	username: string;
	auth_key: string;
	password_hash: string;
	password_reset_token: null;
	email: string;
	client_id: number;
	status: number;
	created_by: null;
	updated_by: string;
	inserted_at: string;
	updated_at: string;
	rate: string;
};

type AuthContextType = {
	user: UserType | null;
	signIn: (data: UserType) => Promise<void>;
	signOut: () => void;
	userStaged: StagedUserType | null;
	setUserStaged: (data: StagedUserType) => void;
	startTime: Date | undefined
	setStartTime: (time: Date) => void
};

SplashScreen.preventAutoHideAsync();

const AuthContext = createContext({} as AuthContextType);

export const useAuth = () => useContext(AuthContext);

type ProviderProps = {
	children: React.ReactNode;
};

export const useProtectedRoute = (
	user: UserType | null,
	isLoading: boolean
) => {
	const router = useRouter();
	const rootSegment = useSegments()[0];
	const [isNavigationReady, setNavigationReady] = useState(false);
	const rootNavigation = useRootNavigation();

	useEffect(() => {
		const unsubscribe = rootNavigation?.addListener("state", () => {
			setNavigationReady(true);
		});
		return function cleanup() {
			if (unsubscribe) {
				unsubscribe();
			}
		};
	}, [rootNavigation]);

	useEffect(() => {
		if (isLoading === false) SplashScreen.hideAsync();

		if (!isNavigationReady) return;

		if (user === undefined) return;
		console.log("running");

		if (
			// If the user is not signed in and the initial segment is not anything in auth group
			!user &&
			rootSegment !== "(auth)"
		) {
			// redirect to signin page
			router.push("/(auth)/login");
		} else if (
			// If the user is signed in and the initial segment is not anything in the app group
			user &&
			rootSegment !== "(dashboard)"
		) {
			// redirect to homepage
			router.replace("/(dashboard)/home");
		}
	}, [user, rootSegment, isNavigationReady, isLoading]);
};

export const AuthProvider = ({ children }: ProviderProps) => {
	const [user, setUser] = useState<UserType | null>(null);
	const [stagedUser, setStagedUser] = useState<StagedUserType | null>(null);
	const { getItem, setItem, removeItem } = useAsyncStorage("@user");
	const [isLoading, setIsLoading] = useState(true);
	const [startTime, setStartTime] =  useState<Date>()

	useEffect(() => {
		getItem().then((data) => {
			if (data) {
				setUser(JSON.parse(data));
				setIsLoading(false);
			} else {
				setIsLoading(false);
			}
		});
	}, []);

	const signIn = async (data: UserType) => {
		try {
			await setItem(JSON.stringify(data));
			setUser(data);
		} catch (error) {
			console.log(error);
		}
	};

	const signOut = () => {
		try {
			removeItem().then(() => {
				setStagedUser(null);
				setUser(null);
				setStartTime(undefined)
			});
		} catch (error) {
			console.log(error);
		}
	};

	const setUserStaged = (data: StagedUserType) => {
		setStagedUser(data);
	};

	useProtectedRoute(user, isLoading);

	return (
		<AuthContext.Provider
			value={{
				user,
				signIn,
				signOut,
				userStaged: stagedUser,
				setUserStaged: setUserStaged,
				startTime,
				setStartTime
			}}
		>
			{children}
		</AuthContext.Provider>
	);
};
