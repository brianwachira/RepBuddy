import React, { useLayoutEffect } from "react";
import {
	DarkTheme,
	DefaultTheme,
	ThemeProvider,
} from "@react-navigation/native";
import { useFonts } from "expo-font";
import { useEffect, useState } from "react";
import { Appearance, ColorSchemeName } from "react-native";
import { customFontsToLoad } from "../config/typography";
import { themeColors } from "../config/theme";
import { Slot, SplashScreen } from "expo-router";
import { AuthProvider } from "../context/auth";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

type Theme = {
	mode: ColorSchemeName;
	system: boolean;
};

// Create a client
const queryClient = new QueryClient();

export default () => {
	const [theme, setTheme] = useState({ mode: Appearance.getColorScheme() });
	const [fontsLoaded] = useFonts(customFontsToLoad);
	const [isReady, setIsReady] = useState(false);

	useEffect(() => {
		const updateTheme = (newTheme: Theme) => {
			let mode: ColorSchemeName;
			if (!newTheme) {
				mode = theme.mode === "dark" ? "light" : "dark";
				newTheme = { mode, system: false };
			} else {
				if (newTheme.system) {
					mode = Appearance.getColorScheme();
					newTheme = { ...newTheme, mode };
				} else {
					newTheme = { ...newTheme, system: false };
				}
			}
			setTheme(newTheme);
		};

		// if the theme of the device changes, update the theme
		Appearance.addChangeListener(({ colorScheme }) => {
			updateTheme({ mode: colorScheme, system: true });
			setTheme({ mode: colorScheme });
		});
	}, [theme.mode]);

	useLayoutEffect(() => {
		setTimeout(() => {
			setIsReady(true);
		}, 500);
	}, []);

	if (!fontsLoaded || !isReady) return <SplashScreen />;

	const lightTheme = {
		...DefaultTheme,
		colors: {
			...DefaultTheme.colors,
			...themeColors.light,
		},
	};

	const darkTheme = {
		...DarkTheme,
		colors: {
			...DarkTheme.colors,
			...themeColors.dark,
		},
	};

	return (
		<ThemeProvider value={theme.mode === "light" ? lightTheme : darkTheme}>
			<AuthProvider>
				<QueryClientProvider client={queryClient}>
					<Slot />
				</QueryClientProvider>
			</AuthProvider>
		</ThemeProvider>
	);
};
