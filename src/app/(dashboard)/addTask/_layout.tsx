import { useTheme } from '@react-navigation/native';
import { Stack } from 'expo-router';
import React from 'react';

export default () => {
  const { colors } = useTheme();

  return (
    <Stack>
      <Stack.Screen
        name="addTask"
        options={{
          headerTintColor: colors.text,
          headerStyle: {
            backgroundColor: colors.background,
          },
        }}
      />
    </Stack>
  );
};