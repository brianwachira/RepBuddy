import { SafeAreaView, StyleSheet, ToastAndroid } from "react-native";
import React, { useEffect, useRef, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import TaskForm from "../../../components/forms/TaskForm";
import { useAuth } from "../../../context/auth";
import { useMutation } from "@tanstack/react-query";
import axios from "axios";
import { formatDate, formatDateToYYYYMMDD } from "../../../util/helpers";
import SuccessModal from "../../../components/modals/SuccessModal";
import { Stack, router } from "expo-router";
import HeaderBackActionLeft from "../../../components/headers/HeaderBackActionLeft";

// Formik initial values
const initialValues = {
	typeOfTask: { label: "", value: "" },
	location: "",
	hcp: { label: "", value: "" },
	product: { label: "", value: "" },
	date: new Date(),
	message: "",
};

const AddTask = () => {
	const { user } = useAuth();
	const [taskType, setTaskType] = useState("");
	const [successMessage, setSuccessMessage] = useState("");

	const [successModalVisible, setSuccessModalVisible] =
		useState<boolean>(false);

	const handleSuccessModal = () => {
		setSuccessModalVisible(true);

		setTimeout(() => {
			setSuccessModalVisible(false);
		}, 3000);
	};

	const handleCloseSuccessModal = () => setSuccessModalVisible(false);

	const handleTaskType = (value: string) => setTaskType(value);

	const productValidation = () =>
		taskType === "1-on-1"
			? Yup.object().optional()
			: Yup.object()
					.shape({
						label: Yup.string().required("HCP is required"),
						value: Yup.string().required("HCP is required"),
					})
					.default(undefined)
					.required("Product is required");

	const hcpValidation = () =>
		taskType === "CME"
			? Yup.object().optional()
			: Yup.object()
					.shape({
						label: Yup.string().required("HCP is required"),
						value: Yup.string().required("HCP is required"),
					})
					.default(undefined)
					.required();

	const messageValidation = () =>
		taskType === "1-on-1"
			? Yup.string().optional()
			: Yup.string().required("Note is required");

	const validationSchema = Yup.object().shape({
		typeOfTask: Yup.object()
			.shape({
				label: Yup.string().required("Task is required"),
				value: Yup.string().required("Task is required"),
			})
			.default(undefined)
			.required(),
		location: Yup.string().required("Location is required"),
		hcp: hcpValidation(),
		product: productValidation(),
		date: Yup.string().required("Date is required"),
		message: messageValidation(),
	});

	const { data, error, isLoading, mutate, mutateAsync } = useMutation({
		mutationKey: ["addTask"],
		mutationFn: async ({
			typeOfTask,
			location,
			hcp,
			product,
			message,
			date,
		}: typeof initialValues) => {
			let params;
			if (taskType === "RTD") {
				params = new URLSearchParams({
					activity: typeOfTask.value,
					activity_type: typeOfTask.value,
					rep_id: user?.id.toString() || "",
					ambassador_id: hcp.value,
					notes: message,
					scheduled_date: formatDateToYYYYMMDD(date),
					location: location,
					product_id: product.value,
				});
			} else if (taskType === "1-on-1") {
				params = new URLSearchParams({
					activity: typeOfTask.value,
					activity_type: typeOfTask.value,
					rep_id: user?.id.toString() || "",
					ambassador_id: hcp.value,
					notes: "",
					scheduled_date: formatDateToYYYYMMDD(date),
					location: location,
					product_id: "",
				});
			} else {
				params = new URLSearchParams({
					activity: typeOfTask.value,
					activity_type: typeOfTask.value,
					rep_id: user?.id.toString() || "",
					ambassador_id: "",
					notes: message,
					scheduled_date: formatDateToYYYYMMDD(date),
					location: location,
					product_id: product.value,
				});
			}

			try {
				const response = await axios.post(
					`${process.env.EXPO_PUBLIC_API_URL}/add-activity?${params}`
				);
				if (response.data.status === false) {
					ToastAndroid.show(response.data.status_message, ToastAndroid.LONG);
				} else {
					setSuccessMessage(`Your new task "${response.data.activity.activity_type} with ${response.data.activity.ambassador_name} in ${response.data.activity.pinned_location} on ${formatDate(response.data.activity.scheduled_date)}" is awaiting approval from manager.`)
					setSuccessModalVisible(true)
				}
			} catch (error) {
				//console.log(error?.response.data);
			}
		},
	});

	const handleSubmit = (values: typeof initialValues) => {
		mutateAsync(values);
	};

	return (
		<SafeAreaView style={styles.container}>
			<Stack.Screen
				options={{
					headerLeft: () => <HeaderBackActionLeft label="Add a task" onPress={()=> router.back()} column/>,
					headerTitle: '',
					headerShadowVisible : false

				}}
			/>
			<Formik
				initialValues={initialValues}
				onSubmit={handleSubmit}
				validationSchema={validationSchema}
			>
				{({ handleSubmit, dirty, isValid }) => (
					<TaskForm
						onSubmit={handleSubmit}
						loading={isLoading}
						dirty={dirty}
						isValid={isValid}
						handleTaskType={handleTaskType}
					/>
				)}
			</Formik>
			<SuccessModal
				visible={successModalVisible}
				title={"Task Created Successfully!"}
				description={successMessage}
				onClose={handleCloseSuccessModal}
			/>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});

export default AddTask;
