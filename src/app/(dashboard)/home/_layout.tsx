import { useTheme } from "@react-navigation/native";
import { Tabs } from "expo-router";
import React from "react";
import { Feather } from "@expo/vector-icons";
import HeaderMain from "../../../components/headers/HeaderMain";
import HeaderTertiary from "../../../components/headers/HeaderTertiary";
import RoutesIcon from "../../../../assets/RoutesIcon";
import ExpensesIcon from "../../../../assets/ExpensesIcon";
import TaskManagerIcon from "../../../../assets/TaskManagerIcon";
import HomeIcon from "../../../../assets/HomeIcon";
import { Platform, StyleSheet, View } from "react-native";
export default () => {
	const { colors } = useTheme();

	return (
		<Tabs
			screenOptions={{
				tabBarShowLabel: true,
				tabBarActiveTintColor: colors.primaryDarker,
				tabBarInactiveTintColor: colors.text,
				tabBarLabelStyle: {
					fontSize: 12,
					fontWeight: "normal",
					fontStyle: "normal",
					fontFamily: "poppinsRegular",
				},
				tabBarIconStyle: {
					marginBottom: 12,
				},
				tabBarStyle: {
					backgroundColor: colors.background,
					...Platform.select({
						ios: {
							shadowColor: "#161616",
							shadowRadius: 2,
							shadowOffset: { width: 0, height: 2 },
							shadowOpacity: 0.05,
							borderTopColor: "rgba(22, 22, 22, 0.05)",
						},
						android: {
							elevation: 0.5,
							borderTopColor: "rgba(22, 22, 22, 0.05)",
						},
					}),
					height: 64,
					//paddingTop: 16,
					paddingBottom: 6,
				},
			}}
		>
			<Tabs.Screen
				name="main"
				options={{
					tabBarIcon: ({ color, focused }) => {
						return focused ? (
							<View
								style={[
									styles.borderTopActive,
									{
										borderColor: colors.primaryDarker,
									},
								]}
							>
								<View style={styles.iconActiveContainer}>
									<HomeIcon color={focused ? colors.primaryDarker : color} />
								</View>
							</View>
						) : (
							<View style={styles.iconInactiveContainer}>
								<HomeIcon color={focused ? colors.primaryDarker : color} />
							</View>
						);
					},
					tabBarLabel: "Home",
					headerTintColor: colors.text,
					headerStyle: {
						backgroundColor: colors.background,
					},
					headerTitleAlign: "center",
					headerTitleContainerStyle: {
						paddingVertical: 16,
						paddingHorizontal: 8,
					},
				}}
			/>
			<Tabs.Screen
				name="taskmanager"
				options={{
					tabBarIcon: ({ color, focused }) => {
						return focused ? (
							<View
								style={[
									styles.borderTopActive,
									{
										borderColor: colors.primaryDarker,
										width:'100%'
									},
								]}
							>
								<View style={styles.iconActiveContainer}>
									<TaskManagerIcon
										color={focused ? colors.primaryDarker : color}
									/>
								</View>
							</View>
						) : (
							<View style={styles.iconInactiveContainer}>
								<TaskManagerIcon
									color={focused ? colors.primaryDarker : color}
								/>
							</View>
						);
					},
					tabBarLabel: "Task Manager",
					headerTintColor: colors.text,
					headerStyle: {
						backgroundColor: colors.background,
					},
					headerTitleAlign: "center",
					headerTitle: () => (
						<HeaderTertiary
							label="Task Manager"
							handlePress={() => console.log("")}
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="expenses"
				options={{
					tabBarIcon: ({ color, focused }) => {
						return focused ? (
							<View
								style={[
									styles.borderTopActive,
									{
										borderColor: colors.primaryDarker,
										width: '80%'
									},
								]}
							>
								<View style={styles.iconActiveContainer}>
									<ExpensesIcon
										color={focused ? colors.primaryDarker : color}
									/>
								</View>
							</View>
						) : (
							<View style={styles.iconInactiveContainer}>
								<ExpensesIcon color={focused ? colors.primaryDarker : color} />
							</View>
						);
					},
					tabBarLabel: "Expenses",
					headerTintColor: colors.text,
					headerStyle: {
						backgroundColor: colors.background,
					},
					headerTitleAlign: "center",
					headerTitle: () => (
						<HeaderTertiary
							label="Expense Tracker"
							handlePress={() => console.log("")}
						/>
					),
				}}
			/>
			<Tabs.Screen
				name="routes"
				options={{
					tabBarIcon: ({ color, focused }) => {
						return focused ? (
							<View
								style={[
									styles.borderTopActive,
									{
										borderColor: colors.primaryDarker,
										width: '60%'

									},
								]}
							>
								<View style={styles.iconActiveContainer}>
									<RoutesIcon color={focused ? colors.primaryDarker : color} />
								</View>
							</View>
						) : (
							<View style={styles.iconInactiveContainer}>
								<RoutesIcon color={focused ? colors.primaryDarker : color} />
							</View>
						);
					},
					tabBarLabel: "Route",
					headerTintColor: colors.text,
					headerStyle: {
						backgroundColor: colors.background,
					},
					headerTitleAlign: "center",
					headerTitle: () => (
						<HeaderTertiary
							label="Routes"
							handlePress={() => console.log("")}
						/>
					),
				}}
			/>
		</Tabs>
	);
};

const styles = StyleSheet.create({
	borderTopActive: {
		borderTopWidth: 2,
		width: "50%",
		height: "100%",
		paddingBottom: 16,
	},
	iconActiveContainer: {
		alignSelf: "center",
		justifyContent: "center",
		alignItems: "center",
		paddingTop: 12,
	},
	iconInactiveContainer: {
		marginTop: 20,
	},
});
