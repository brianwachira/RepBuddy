import React, { useState } from "react";
import { useTheme } from "@react-navigation/native";
import {
	Pressable,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from "react-native";
import { useAuth } from "../../../context/auth";
import MainContainer from "../../../components/shared/Container";
import Space from "../../../components/shared/Space";
import Row from "../../../components/shared/Row";
import StyledText from "../../../components/shared/StyledText";
import CircularProgress from "react-native-circular-progress-indicator";
import LogoutModal from "../../../components/modals/LogoutModal";
import { Stack } from "expo-router";
import HeaderMain from "../../../components/headers/HeaderMain";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import ActivitiesList from "../../../components/list/ActivitiesList";

const Home = () => {
	const { user, signOut, startTime, setStartTime } = useAuth();

	const handleStartTime = () => {
		setStartTime(new Date());
	};

	const { colors } = useTheme();

	const [logoutModalVisible, setLogoutModalVisible] = useState<boolean>(false);

	const ToggleLogoutModal = () => {
		setLogoutModalVisible(!logoutModalVisible);
	};

	const currentDay = new Date().toISOString().slice(0, 10);

	const { isLoading, error, data } = useQuery({
		queryKey: ["activitiesToday"],
		queryFn: async () => {
			const response = await axios.get(
				`${process.env.EXPO_PUBLIC_API_URL}/fetch-activities?rep_id=${user?.id}&start_date=${currentDay}`
			);

			return response.data;
		},
	});

	console.log(data?.activities);
	return (
		<MainContainer preset="fixed">
			<Stack.Screen
				options={{
					headerTitle: () => <HeaderMain toggleProfile={ToggleLogoutModal} />,
					headerShadowVisible: false,
				}}
			/>
			{startTime ? (
				<>
					<View
						style={[
							styles.container,
							{ borderBottomColor: colors.primaryLight },
						]}
					>
						<Space size={24} />
						<Row>
							<View style={styles.instructions}>
								<StyledText font="medium" size="base">
									Way to go {user?.names}!
								</StyledText>
								<StyledText font="regular" size="base">
									You checked in at{" "}
									{startTime.toLocaleString("en-US", {
										hour: "numeric",
										minute: "numeric",
										hour12: true,
									})}{" "}
									today. Your tasks await, you've got this.
								</StyledText>
							</View>
							<CircularProgress
								value={100}
								radius={40}
								duration={2000}
								progressValueColor={colors.secondaryDarker}
								maxValue={100}
								valueSuffix={"%"}
								title={"Complete"}
								titleColor={colors.secondaryDarker}
								titleStyle={styles.titleStyle}
								valueSuffixStyle={styles.suffixStyle}
								activeStrokeColor={colors.secondaryDarker}
								inActiveStrokeColor={colors.progressInactive}
							/>
						</Row>
						<Space size={24} />
					</View>
					<Space size={24} />
					{isLoading ? (
						<StyledText>...loading</StyledText>
					) : (
						<ActivitiesList item={data} />
					)}
				</>
			) : (
				<>
					<View
						style={[
							styles.container,
							{ borderBottomColor: colors.primaryLight },
						]}
					>
						<Space size={24} />
						<Row>
							<View style={styles.instructions}>
								<StyledText font="medium" size="base">
									Hi {user?.names}!
								</StyledText>
								<StyledText font="regular" size="base">
									Unveil your tasks by tapping the button on your right
								</StyledText>
							</View>
							<TouchableOpacity
								style={[
									styles.startButton,
									{ backgroundColor: colors.secondary },
								]}
								onPress={() => handleStartTime()}
							>
								<StyledText variant="white" size="base" font="semiBold">
									Start
								</StyledText>
							</TouchableOpacity>
						</Row>
						<Space size={24} />
					</View>
				</>
			)}
			<View>
				<LogoutModal visible={logoutModalVisible} onClose={ToggleLogoutModal} />
			</View>
		</MainContainer>
	);
};

const styles = StyleSheet.create({
	container: {
		borderBottomWidth: 1,
	},
	instructions: {
		flex: 0.85,
	},
	startButton: {
		width: 60,
		height: 60,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 30,
		elevation: 10,
	},
	suffixStyle: {
		fontFamily: "poppinsMedium",
		fontWeight: "500",
		fontSize: 16,
		marginTop: 6,
	},
	titleStyle: {
		fontFamily: "poppinsMedium",
		fontSize: 6,
		marginTop: -8,
	},
});

export default Home;
