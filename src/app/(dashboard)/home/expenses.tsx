import React from "react";
import MainContainer from "../../../components/shared/Container";
import StyledText from "../../../components/shared/StyledText";
import ExpensesIcon from "../../../../assets/ExpensesIcon";

const Expenses = () => {
    return ( 
        <MainContainer>
            <StyledText>hello</StyledText>
            <ExpensesIcon color="black"/>
        </MainContainer>
     );
}

export default Expenses