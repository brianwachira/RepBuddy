import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { useAuth } from "../../context/auth";
import MainContainer from "../../components/shared/Container";
import Space from "../../components/shared/Space";
import LoginForm from "../../components/forms/LoginForm";
import { useMutation } from "@tanstack/react-query";
import { router } from "expo-router";
import axios from "axios";
import { ToastAndroid } from "react-native";

// formik initial values
const initialValues = {
	email: "",
	password: "",
};

//formik validation schema
const validationSchema = Yup.object().shape({
	email: Yup.string().required("Email is required"),
	password: Yup.string().required("Password is required"),
});

export default function Login() {
	const { user, setUserStaged } = useAuth();

	const { data, error, isLoading, mutate, mutateAsync } = useMutation({
		mutationKey: ["login"],
		mutationFn: async ({
			email,
			password,
		}: {
			email: string;
			password: string;
		}) => {
			const params = new URLSearchParams({
				phone: email,
				password,
			});

			try {
				const response = await axios.get(
					`${process.env.EXPO_PUBLIC_API_URL}/login?${params}`
				);

				if (response.data.status === true) {
					setUserStaged(response.data);
					router.push("/verification");
				} else {
					ToastAndroid.show(response.data.status_message, ToastAndroid.LONG);
				}
			} catch (error) {
				return error;
			}
		},
	});
	const handleSubmit = (values: { email: string; password: string }) => {
		mutateAsync(values);
	};
	return (
		<MainContainer>
			<Space size={16} />
			<Formik
				initialValues={initialValues}
				onSubmit={handleSubmit}
				validationSchema={validationSchema}
			>
				{({ handleSubmit }) => (
					<LoginForm onSubmit={handleSubmit} loading={isLoading} />
				)}
			</Formik>
		</MainContainer>
	);
}
