import React from "react";
import { View, Text, StyleSheet } from "react-native";
import MainContainer from "../../components/shared/Container";
import StyledText from "../../components/shared/StyledText";
import { Stack, router } from "expo-router";
import HeaderBackActionLeft from "../../components/headers/HeaderBackActionLeft";
import Space from "../../components/shared/Space";

const ForgotPassword = () => {
    return ( 
        <MainContainer>
            <Stack.Screen
                options={{
                    headerLeft: () => (
                        <HeaderBackActionLeft onPress={() => router.push('resetpassword')} label="Back to Login"/>
                    )
                }}
            />
            <View style={styles.main}>
                <StyledText size="lg" font="light" style={styles.textCenter}>Forgot Password</StyledText>
                <Space size={8}/>
                <View style={styles.instructions}>
                    <Text  style={styles.textCenter}>
                        <StyledText size="base" font="light">We've sent a password resest code to the phone number </StyledText>
                        <StyledText size="base" font="light" variant="primaryDarker">0712345679.</StyledText>
                        <StyledText size="base" font="light"> Use the link to reset the password</StyledText>
                    </Text>

                </View>
            </View>
        </MainContainer>
     );
}

const styles = StyleSheet.create({
    instructions: {
        maxWidth: 310,

    },
	main: {
		flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
	},
    textCenter: {
        textAlign: 'center'
    }
})

export default ForgotPassword