import React, { useEffect, useState } from "react";
import MainContainer from "../../components/shared/Container";
import { Alert, StyleSheet, Text, ToastAndroid, View } from "react-native";
import StyledText from "../../components/shared/StyledText";
import Space from "../../components/shared/Space";
import OTPInput from "../../components/shared/OTPInput";
import { useMutation } from "@tanstack/react-query";
import WelcomeModal from "../../components/modals/WelcomeModal";
import axios from "axios";
import { useAuth } from "../../context/auth";

const Verification = () => {
	const { userStaged, signIn } = useAuth();
	const [otp, setOTP] = useState("");
	const [isPinReady, setIsPinReady] = useState(false);

	const [welcomeModalVisible, setWelcomeModalVisible] =
		useState<boolean>(false);

	const showWelcomeModal = () => {
		setWelcomeModalVisible(true);
	};

	const maximumLength = 4;
	console.log(userStaged?.otp);
	const verifyOtp = async (otp: string) => {
		const params = new URLSearchParams({
			totp: otp,
			phone: userStaged?.data.msisdn || "",
		});

		axios
			.get(`${process.env.EXPO_PUBLIC_API_URL}/verify-otp?${params}`)
			.then((res) => {
				if (res.data.status == false) {
					throw new Error(res.data.status_message);
				} else {
					showWelcomeModal();
					if (userStaged !== null) {
						setTimeout(() => {
							signIn({
								...userStaged?.data,
								rate: userStaged?.rate || "",
							});
						}, 3000);
					}
				}
			});
	};

	const { data, error, isLoading, mutate } = useMutation({
		mutationKey: ["verify", "verifyOtp"],
		mutationFn: async (otp: string) => {
			const params = new URLSearchParams({
				totp: otp,
				phone: userStaged?.data.msisdn || "",
			});
			try {
				const response = await axios.get(
					`${process.env.EXPO_PUBLIC_API_URL}/verify-otp?${params}`
				);
				if (response.data.status === false) {
					ToastAndroid.show(response.data.status_message, ToastAndroid.LONG);
				} else {
					showWelcomeModal()
					if(userStaged !== null) {
						setTimeout(() => {
							signIn({
								...userStaged?.data,
								rate: userStaged?.rate || "",

							})
						},3000)
					}
				}
				return response;
			} catch (error) {
				return error;
			}
		},
	});

	const handleSubmit = () => {
		mutate(otp);
	};

	useEffect(() => {
		if (isPinReady === true) {
			handleSubmit();
		}
	}, [isPinReady]);

	return (
		<MainContainer>
			<View style={styles.main}>
				<StyledText size="lg" font="light" style={styles.textCenter}>
					Verification
				</StyledText>
				<Space size={8} />
				<View style={styles.instructions}>
					<Text style={styles.textCenter}>
						<StyledText size="base" font="light">
							We’ve sent a verification code the Phone Number{" "}
						</StyledText>
						<StyledText size="base" font="light" variant="primaryDarker">
							{" "}
							0701 ****88.
						</StyledText>
						<StyledText size="base" font="light">
							{" "}
							Enter the code below
						</StyledText>
					</Text>
				</View>
				<Space size={24} />

				<StyledText size="base" variant="secondaryDarker">
					{error as unknown as string}
				</StyledText>

				<OTPInput
					code={otp}
					setCode={setOTP}
					maximumLength={maximumLength}
					setIsPinReady={setIsPinReady}
					isLoading
				/>
			</View>
			<View>
				<WelcomeModal
					visible={welcomeModalVisible}
					onClose={showWelcomeModal}
				/>
			</View>
		</MainContainer>
	);
};

const styles = StyleSheet.create({
	instructions: {
		maxWidth: 330,
	},
	main: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	textCenter: {
		textAlign: "center",
	},
});
export default Verification;
