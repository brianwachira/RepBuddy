import React from "react";
import { Stack } from "expo-router";
import HeaderIcon from "../../components/headers/HeaderIcon";

export default () => {
	return (
		<Stack>
			<Stack.Screen
				name="login"
				options={{
					headerTitle: () => <HeaderIcon />,
					headerShadowVisible: false,
					headerTitleAlign: "center",
				}}
			/>
			<Stack.Screen
				name="forgotpassword"
				options={{
					headerTitle: () => null,
					headerShadowVisible: false,
				}}
			/>
			<Stack.Screen
				name="resetpassword"
				options={{
					headerTitle: () => <HeaderIcon />,
					headerShadowVisible: false,
					headerTitleAlign: "center",
					headerBackVisible: false
				}}
			/>
			<Stack.Screen
				name="verification"
				options={{
					headerTitle: () => <HeaderIcon />,
					headerShadowVisible: false,
					headerTitleAlign: "center",
					headerBackVisible: false
				}}
			/>
		</Stack>
	);
};
