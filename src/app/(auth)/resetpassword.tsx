import React from "react";
import {Formik} from 'formik';
import * as Yup from 'yup';
import { useAuth } from "../../context/auth";
import MainContainer from "../../components/shared/Container";
import Space from "../../components/shared/Space";
import { useMutation } from "@tanstack/react-query";
import ResetPasswordForm from "../../components/forms/ResetPasswordForm";

// formik initial values.
const initialValues = {
    password: '',
    confirmPassword: '',
};

// formik validation schema.
const validationSchema = Yup.object().shape({
    password: Yup.string().required('Password is required'),
    confirmPassword: Yup.string().required('Password is required').oneOf([Yup.ref('password')], 'Passwords must match')
})

const ResetPassword = () => {

    const resetPassword = async (password: string) => {
        const response = await fetch('/data')
        const data = await response.json()
        return data

    }
    const { data, error, isLoading, mutate, mutateAsync} = useMutation(resetPassword)
  
    const handleSubmit = (values: { password: string, confirmPassword: string}) => {

    console.log(values)
    mutate(values.password)
  }
    return ( 
        <MainContainer>
            <Space size={16}/>
            <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}>
            {({handleSubmit}) => (
                <ResetPasswordForm
                    onSubmit={handleSubmit}
                    loading={isLoading}/>
            )}
          </Formik>
        </MainContainer>
     );
}

export default ResetPassword