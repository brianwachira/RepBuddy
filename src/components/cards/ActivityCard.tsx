import React from "react";
import { useTheme } from "@react-navigation/native";
import { Pressable, StyleSheet, TouchableOpacity, View } from "react-native";
import Row from "../shared/Row";
import { Ionicons } from "@expo/vector-icons";
import { IActivity } from "../../../global";
import StyledText from "../shared/StyledText";
import Space from "../shared/Space";

interface ActivityCardProps {
	item: IActivity;
	onPressCheckBox: () => void;
	handlePress: () => void;
}

/**
 * 
 * @param item: IActivity 
 * @param onPressCheckBox: () => void 
 * @param handlePress: () => void 
 * @returns ActivityCard component
 */

const ActivityCard = ({
	item,
	onPressCheckBox,
	handlePress,
}: ActivityCardProps) => {
	const { colors } = useTheme();

	return (
		<View style={[styles.container, styles.shadow]}>
			<Row style={styles.rowStart}>
				<Pressable
					style={[
						styles.checkBox,
						{
							borderColor: colors.textSecondary2,
							backgroundColor: colors.background,
						},
					]}
					onPress={onPressCheckBox}
				>
					<Ionicons name="checkmark" size={14} color={colors.primaryDarker} />
				</Pressable>
				<Space size={7} horizontal />
				<TouchableOpacity onPress={handlePress}>
					<StyledText size="base" font="light" variant="textSecondary">
						{item.activity_type} with {item.ambassador_name}
					</StyledText>
					<Space size={8} />
					<Row style={styles.rowEnd}>
						<Ionicons
							name="location-sharp"
							size={15}
							color={colors.primaryDarker}
						/>
						<Space size={4.5} horizontal />
						<StyledText variant="primaryDarker">{item.location}</StyledText>
					</Row>
				</TouchableOpacity>
			</Row>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 10,
		paddingVertical: 9,
		borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,

		elevation: 3,

	},
	shadow: {
	},
	checkBox: {
		width: 20,
		height: 20,
		borderWidth: 2,
		borderRadius: 4,
		alignItems: "center",
		justifyContent: "center",
	},
	rowStart: {
		justifyContent: "flex-start",
		alignItems: "flex-start",
	},
	rowEnd: {
		justifyContent: "flex-start",
		alignItems: "center",
	},
});

export default ActivityCard;
