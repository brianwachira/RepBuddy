import { useTheme } from "@react-navigation/native";
import { IActivity } from "../../../global";
import {
	Modal,
	View,
	StyleSheet,
	Pressable,
	TouchableOpacity,
} from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import StyledText from "../shared/StyledText";
import { formatDate } from "../../util/helpers";
import Row from "../shared/Row";
import { router } from "expo-router";

type ActivityModalProps = {
	visible: boolean;
	onClose: () => void;
	item: IActivity | undefined;
	onPressCheckBox: () => void;
};

/**
 * 
 * @param visible: boolean 
 * @param onClose: () => void 
 * @param item: IActivity | undefined
 * @param onPressCheckBox: () => void 
 * @returns ActivityModal component
 */

const ActivityModal = ({
	visible,
	onClose,
	item,
	onPressCheckBox,
}: ActivityModalProps) => {
	const { colors } = useTheme();
	return (
		<View>
			<Modal animationType="slide" transparent visible={visible}>
				<Pressable style={styles.startView} onPress={onClose}>
					<View style={[styles.modalView, { backgroundColor: colors.bg }]}>
						<Pressable style={styles.modalHeader} onPress={onPressCheckBox}>
							<View
								style={[
									styles.checkBox,
									{
										borderColor: colors.textSecondary2,
										backgroundColor: colors.background,
									},
								]}
							>
								<Ionicons
									name="checkmark"
									size={14}
									color={colors.primaryDarker}
								/>
							</View>
							<StyledText size="sm" font="medium" variant="primaryDarker">
								MARK AS COMPLETE
							</StyledText>
						</Pressable>
						<View>
							<StyledText size="md" font="light" variant="textSecondary">
								{item?.activity_type} with {item?.ambassador_name}
							</StyledText>
							<StyledText size="sm" font="light" variant="textSecondary">
								At {item?.location}
							</StyledText>
							<StyledText size="sm" font="light" variant="textSecondary">
								{formatDate(item?.scheduled_date || new Date())}
							</StyledText>
						</View>
						<Row>
							<TouchableOpacity
								onPress={() => router.push(`reschedule/${item?.id}`)}
							>
								<StyledText
									size="sm"
									font="medium"
									variant="primaryDarker"
									underline
								>
									RESCHEDULE
								</StyledText>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => router.push(`cancel/${item?.id}`)}
							>
								<StyledText
									size="sm"
									font="medium"
									variant="primaryDarker"
									underline
								>
									CANCEL
								</StyledText>
							</TouchableOpacity>
						</Row>
					</View>
				</Pressable>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	startView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0, 0, 0, 0.7)",
		paddingTop: 20,
	},
	modalView: {
		width: "90%",
		borderRadius: 10,
		padding: 16,
	},
	modalContent: {
		flex: 1,
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	modalHeader: { flexDirection: "row", alignItems: "center" },
	modalBody: {
		width: "95%",
	},
	checkBox: {
		width: 20,
		height: 20,
		borderWidth: 2,
		borderRadius: 4,
		alignItems: "center",
		justifyContent: "center",
	},
});

export default ActivityModal;
