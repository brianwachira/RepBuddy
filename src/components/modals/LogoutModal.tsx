import { useTheme } from "@react-navigation/native";
import React from "react";
import { Modal, View, StyleSheet, Pressable } from "react-native";
import StyledText from "../shared/StyledText";
import { useAuth } from "../../context/auth";
import Space from "../shared/Space";
import StyledButton from "../buttons";

type LogoutModalProps = {
    visible: boolean;
    onClose: () => void;
}

/**
 * 
 * @param visible: boolean
 * @param onClose: () => void
 * @returns LogoutModal
 */

const LogoutModal = ({ visible, onClose }: LogoutModalProps) => {
    const { colors } = useTheme()
    const { user, signOut } = useAuth()

    return (
        <View>
            <Modal 
                animationType="slide"
                transparent
                visible={visible}
                onRequestClose={onClose}
                >
                    <Pressable style={styles.startView} onPress={onClose}>
                        <View  style={[styles.modalView, { backgroundColor: colors.bg }]}>
                            <StyledText variant="gray" size="base" font="light">{user?.names}</StyledText>
                            <StyledText variant="gray" size="base" font="light">{user?.email}</StyledText>
                            <Space size={48}/>
                            <StyledButton variant="secondary" label="LOGOUT" onPress={signOut}/>
                        </View>
                    </Pressable>
            </Modal>
        </View>
    )
};

const styles = StyleSheet.create({
	startView: {
		flex: 1,
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "rgba(0, 0, 0, 0.7)",
        paddingTop: 20,
	},
	modalView: {
		width: "90%",
		borderRadius: 10,
		padding: 16,
		alignItems: "center",
	},
	modalContent: {
		flex: 1,
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
});

export default LogoutModal