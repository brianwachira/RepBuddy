import { useTheme } from "@react-navigation/native";
import { Image } from "expo-image";
import React from "react";
import { Modal, StyleSheet, View } from "react-native";
import StyledText from "../shared/StyledText";

type WelcomeModalProps = {
	visible: boolean;
	onClose: () => void;
};

/**
 * 
 * @param visible: boolean 
 * @param onClose: () => void 
 * @returns WelcomeModal component
 */

const WelcomeModal = ({ visible, onClose }: WelcomeModalProps) => {
	const { colors } = useTheme();

	const handleWelcome = () => {
		onClose();
	};

	return (
		<View style={styles.centeredView}>
			<Modal
				animationType="slide"
				transparent
				visible={visible}
				onRequestClose={onClose}
			>
				<View style={styles.centeredView}>
					<View  style={[styles.modalView, { backgroundColor: colors.bg }]}>
                        <Image style={styles.image} source={require("../../../assets/startday.gif")} contentFit="contain"/>
                        <StyledText size="md" font="light" variant="black">Have a great day ahead! </StyledText>
                    </View>
				</View>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0, 0, 0, 0.7)",
	},
	modalView: {
		width: "90%",
		//height: "50%",
		borderRadius: 10,
		padding: 16,
		alignItems: "center",
	},
	modalContent: {
		flex: 1,
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	image: {
		width: '100%',
		height: 262
	}
});

export default WelcomeModal
