import { useTheme } from "@react-navigation/native";
import React from "react";
import { Modal, Pressable, View, StyleSheet } from "react-native";
import StyledText from "../shared/StyledText";
import CheckMarkFilledIcon from "../../../assets/CheckMarkFilledIcon";
import Space from "../shared/Space";

type SuccessModalProps = {
	visible: boolean;
	title: string;
	description: string;
	onClose: () => void;
};

/**
 * 
 * @param visible: boolean 
 * @param title: string
 * @param description: string 
 * @param onClose: () => void
 * @returns SuccessModal
 */

const SuccessModal = ({
	title,
	description,
	visible,
	onClose,
}: SuccessModalProps) => {
	const { colors } = useTheme();
	return (
		<View>
			<Modal animationType="slide" transparent visible={visible}>
				<Pressable style={styles.startView} onPress={onClose}>
					<View style={[styles.modalView, { backgroundColor: colors.bg }]}>
						<View style={styles.modalHeader}>
							<CheckMarkFilledIcon />
							<StyledText size="md" variant="black" font="medium">
								{title}
							</StyledText>
						</View>
                        <View style={styles.modalBody}>
						<StyledText size="md" variant="black" font="light">
							{description}
						</StyledText>
                        </View>
					</View>
				</Pressable>
			</Modal>
		</View>
	);
};

const styles = StyleSheet.create({
	startView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "rgba(0, 0, 0, 0.7)",
		paddingTop: 20,
	},
	modalView: {
		width: "90%",
		borderRadius: 10,
		padding: 16,
	},
	modalContent: {
		flex: 1,
		width: "100%",
		justifyContent: "center",
		alignItems: "center",
	},
	modalHeader: { flexDirection: "row", alignItems: "center" },
    modalBody: {
        width: '95%'
    }
});

export default SuccessModal;
