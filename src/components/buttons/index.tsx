import { useTheme } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import StyledText from '../shared/StyledText';

type StyledButtonProps = {
  onPress: () => void;
  label: string;
  variant? : 'primary' | 'secondary' | 'disabled'
  loading? : boolean
};


/**
 * 
 * @returns Button component
 * @param label: string
 * @param onPress: () => void
 * @param variant: 'primary' | 'secondary' | 'disabled'
 */

const StyledButton = ({ onPress, label, variant='primary', loading= false }: StyledButtonProps) => {
  const { colors } = useTheme();
  const variants : Record<NonNullable<StyledButtonProps['variant']>, {paddingVertical: number, backgroundColor: string}> = {
    primary: {
        ...styles.primary,
        backgroundColor: colors.primaryDarker,
        },
    secondary: {
        ...styles.secondary,
    backgroundColor: colors.secondary},
    disabled: {
        ...styles.primary,
        backgroundColor: colors.disabled,


    }
  }
  const textSizes: Record<NonNullable<StyledButtonProps['variant']>, 'md'| 'base'> = {
    primary: 'md',
    secondary: 'base',
    disabled: 'md'
  }

  const textFontSizes: Record<NonNullable<StyledButtonProps['variant']>, 'bold' | 'semiBold' | 'light'> = {
    primary: 'bold',
    secondary: 'semiBold',
    disabled: 'light'
  }

  const textColors: Record<NonNullable<StyledButtonProps['variant']>, 'white' | 'gray'> = {
    primary: 'white',
    secondary: 'white',
    disabled: 'gray'
  }

  return (
    <TouchableOpacity
      style={[styles.button, variants[variant]]}
      onPress={onPress}
      testID="primaryButton"
      disabled={loading}
      
    >
      <StyledText size={textSizes[variant]} font={textFontSizes[variant]} variant={textColors[variant]} style={[styles.buttonText]}>
        {label}
      </StyledText>
    </TouchableOpacity>
  );
};

export default StyledButton;

const styles = StyleSheet.create({
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  buttonText: {
    textTransform: 'uppercase',
  },
  primary: {
    paddingVertical: 16,
  },
  secondary: {
    paddingVertical: 6,
  }
});
