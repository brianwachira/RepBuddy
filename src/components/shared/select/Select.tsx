import React, { useContext, useState } from "react";
import { useTheme } from "@react-navigation/native";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import SelectItem from "./Select.Item";
import StyledText from "../StyledText";

export const SelectContext = React.createContext({
	onValueChange: (label: string, value: string) => {},
	selectedValue: { label: "", value: "" },
});

export interface SelectProps {
	children: React.ReactNode;
	placeholder: string;
	selectedValue: { label: string; value: string };
	onValueChange: (label: string, value: string) => void;
	showError?: boolean;
	error?: string;
}

export const useSelect = () => useContext(SelectContext);

/**
 * 
 * @param children: React.ReactNode
 * @param placeholder: string
 * @param selectedValue:  { label: string; value: string }
 * @param onValueChange: (label: string, value: string) => void
 * @param showError?: boolean
 * @param error?: string
 * @returns Select component
 */

const Select = ({
	onValueChange,
	selectedValue,
	children,
	placeholder,
	showError,
	error,
}: SelectProps) => {
	const { colors } = useTheme();
	const [optionsVisible, setOptionsVisible] = useState<boolean>(false);

	const handleSelect = (label: string, value: string) => {
		setOptionsVisible(!optionsVisible);
		onValueChange(label, value);
	};

	const contextValue = React.useMemo(() => {
		return {
			onValueChange: handleSelect,
			selectedValue: selectedValue,
		};
	}, [handleSelect, selectedValue]);
	return (
		<>
			{optionsVisible === false ? (
				<View>
					{selectedValue.label?.length > 1 ? (
						<TouchableOpacity
							style={[
								styles.inputStyleFilled,
								{
									borderBottomColor: colors.primaryDarker,
								},
							]}
							onPress={() => setOptionsVisible(!optionsVisible)}
						>
							<StyledText size="sm" font="light" variant="primaryDarker">
								{placeholder}
							</StyledText>
							<StyledText size="md" font="light" variant="textPrimary">
								{selectedValue.label}
							</StyledText>
						</TouchableOpacity>
					) : (
						<TouchableOpacity
							style={[
								styles.inputStyle,
								{
									backgroundColor: colors.whiteSecondary,
									borderBottomColor: colors.primaryLight,
								},
							]}
							onPress={() => setOptionsVisible(!optionsVisible)}
						>
							<StyledText size="md" font="regular" variant="placeholder">
								{placeholder}
							</StyledText>
						</TouchableOpacity>
					)}
				</View>
			) : (
				<View style={styles.dropDownItemContainer}>
					<TouchableOpacity
						style={[
							styles.inputStyleFilled,
							{
								backgroundColor: colors.background,
								borderBottomColor: colors.primaryDarker,
							},
						]}
						onPress={() => setOptionsVisible(!optionsVisible)}
					>
						<StyledText size="sm" font="light" variant="primaryDarker">
							{placeholder}
						</StyledText>
						<StyledText size="md" font="light" variant="textPrimary">
							{selectedValue.label}
						</StyledText>
					</TouchableOpacity>
					<SelectContext.Provider value={contextValue}>
						{children}
					</SelectContext.Provider>
				</View>
			)}
			{showError && (
				<StyledText variant="secondary" size="sm">
					{error}
				</StyledText>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	inputStyle: {
		borderBottomWidth: 1,
		padding: 16,
	},
	inputStyleFilled: {
		padding: 14,
		paddingBottom: 8,
		borderBottomWidth: 2,
	},
	dropDownItemContainer: {
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,

		elevation: 2,
	},
});

Select.displayName = "Select";

// Exported components now can be used as <Select> and <Select.Item>
export default Object.assign(Select, { Item: SelectItem });
