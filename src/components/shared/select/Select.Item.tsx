import { Pressable, StyleSheet, TouchableOpacity, View } from "react-native";
import { useTheme } from "@react-navigation/native";
import StyledText from "../StyledText";
import React from "react";
import Space from "../Space";
import { SelectContext, useSelect } from "./Select";

type SelectItemProps = {
	label: string;
	children: React.ReactNode;
	value: string
};

/**
 * 
 * @param label: string
 * @param children: React.ReactNode
 * @param value: string 
 * @returns SelectItem component
 */
const SelectItem = ({ children, value, label }: SelectItemProps) => {
	const { colors } = useTheme();

	const {onValueChange, selectedValue} = useSelect()
	return (
		<TouchableOpacity style={styles.container} onPress={() => onValueChange(label, value)}>
			<Pressable style={[styles.radioButton,{borderColor: colors.black, backgroundColor: colors.background}]}>
				<View style={[styles.radioButtonInner,{ backgroundColor: selectedValue.value === value? colors.primaryDarker: colors.background}]} />
			</Pressable>
			<Space size={8} horizontal/>
			<StyledText size="base" font="light" variant="textSecondary4">
				{children}
			</StyledText>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	radioButton: {
		width: 16.615,
		height: 16.615,
		borderWidth: 2,
		borderRadius: 8.3075,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: -3.5
	},
	radioButtonInner: {
		width: 10,
		height: 10,
		borderRadius: 5,
	},
	container: {
		padding: 16,
		flexDirection: 'row',
		alignItems: 'center'
	},
});

SelectItem.displayName = "SelectItem";

// Exported components can now be used as <SelectItem>
export default Object.assign(SelectItem);
