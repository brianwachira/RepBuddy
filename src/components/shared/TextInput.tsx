import React from 'react';
import { forwardRef, LegacyRef } from 'react';
import { TextInput as NativeTextInput,StyleSheet,
    StyleProp,
    TextStyle,
    TextInputProps, } from 'react-native';

interface textInputProps extends TextInputProps {
    error?: boolean;
}

/**
 * @returns TextInput component
 * @param error?: boolean
 */

const TextInput = forwardRef((props: textInputProps, ref) => {
    const textInputStyle: StyleProp<TextStyle> = [
        props.style,
        props.error && styles.borderDanger,
      ];
      return (
        <NativeTextInput
          style={textInputStyle}
          {...props}
          ref={ref as LegacyRef<NativeTextInput> | undefined}
        />
      );
})

const styles = StyleSheet.create({
    borderDanger: {
      borderBottomColor: 'red',
      borderBottomWidth: 1,
      height: 40,
    },
  });

export default TextInput