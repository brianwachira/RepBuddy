import React from 'react';
import { View } from 'react-native';

type SpaceProps = {
  size: number;
  horizontal?: boolean;
};

/**
 * @param size: number;
 * @param horizontal?: boolean;
 * @returns Space component;
 */

const Space = ({ size, horizontal = false }: SpaceProps) => {
  const horizontalStyle = { width: size };
  const verticalStyle = { height: size };

  return <View style={horizontal ? horizontalStyle : verticalStyle} />;
};

export default Space;