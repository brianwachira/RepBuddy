import { useTheme } from '@react-navigation/native';
import React from 'react';
import type { StyleProp, TextStyle } from 'react-native';
import { StyleSheet, Text as NativeText, } from 'react-native';
import { typography } from '../../config/typography';

type StyledTextProps = {
  children: React.ReactNode;
  size?: 'xs' | 'sm' | 'base' | 'md' | 'lg'| 'xl';
  font?: 'bold' | 'regular' | 'medium' | 'semiBold' | 'light';
  variant?: 'text' | 'primary'  | 'secondary' | 'primaryDarker' | 'secondaryDarker' | 'white' | 'gray' | 'black' | 'textSecondary' | 'textPrimary' | 'textSecondary4' | 'placeholder' | 'textSecondary2';
  style?: StyleProp<TextStyle>;
  underline?: boolean
};

/**
 * @returns Text component
 * @param children: React.ReactNode - text
 * @param size: xs:10 | sm:12 | base:14 | md:16 | lg:24
 * @param fonts: bold:700 | regular:300 | medium:500 | semibold:600 | light:400
 * @param variant: text | primary | secondary | primaryDarker | secondaryDarker | white | gray | black | textSecondary | textPrimary | textSecondary4 | placeholder | textSecondary2
 * @param style: StyleProp<TextStyle> - custom style
 * @param underline?: boolean
 */

const StyledText = ({
  style,
  size = 'md',
  font = 'regular',
  variant = 'text',
  underline = false,
  ...rest
}: StyledTextProps & NativeText['props']) => {
  const { colors } = useTheme();

  const { primary } = typography;

  const sizes: Record<NonNullable<StyledTextProps['size']>, number> = {
    xs: 10,
    sm: 12,
    base: 14,
    md: 16,
    lg: 24,
    xl: 36,
  };
  const fonts: Record<NonNullable<StyledTextProps['font']>, string> = {
    bold: primary[700],
    regular: primary[300],
    medium: primary[500],
    semiBold: primary[600],
    light: primary[400],
  };
  const variants: Record<NonNullable<StyledTextProps['variant']>, string> = {
    text: colors.text,
    primary: colors.primary,
    secondary: colors.secondary,
    primaryDarker: colors.primaryDarker,
    secondaryDarker: colors.secondaryDarker,
    white: colors.textWhite,
    gray: colors.gray3,
    black: colors.black,
    textSecondary: colors.textSecondary,
    textPrimary: colors.textPrimary,
    textSecondary4: colors.textSecondary4,
    placeholder: colors.placeholder,
    textSecondary2: colors.textSecondary2,
  };



  return (
    <NativeText
      style={StyleSheet.compose({ fontSize: sizes[size], fontFamily: fonts[font], color: variants[variant], textDecorationColor: underline? variants[variant] : 'transparent', textDecorationLine: underline ? 'underline' : 'none' }, style)}
      {...rest}
    />
  );
};

const styles = StyleSheet.create({
  textUnderline: {
    textDecorationLine: 'underline',
    textDecorationColor: 'none',
  }
})
export default StyledText;