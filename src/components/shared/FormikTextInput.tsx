import React from "react";
import { useTheme } from "@react-navigation/native";
import { StyleSheet, TextInputProps, View } from "react-native";
import { useField } from "formik";
import TextInput from "./TextInput";
import Ionicons from "@expo/vector-icons/Ionicons";
import StyledText from "./StyledText";
import { typography } from "../../config/typography";

interface FormikTextInputProps extends TextInputProps {
	name: string;
	isPasswordField?: boolean;
	togglePassword?: () => void;
	handleValueChange?: (value: string) => void;
}

/**
 * 
 * @param name: string
 * @param isPasswordField?: boolean
 * @param togglePassword?: () => void
 * @param handleValueChange?: (value: string) => void
 * @returns FormikTextInput component
 */

const FormikTextInput = ({
	name,
	isPasswordField,
	togglePassword,
	secureTextEntry,
	handleValueChange,
	...rest
}: FormikTextInputProps) => {
	const [field, meta, helpers] = useField(name);

	// Check if the field is touched and error message is present
	const showError: boolean = meta.touched && meta.error ? true : false;

	const { colors } = useTheme();
	const { primary } = typography;
	return (
		<View style={{ position: "relative" }}>
			<TextInput
				onChangeText={(value: any) => {
					helpers.setValue(value);
					if (handleValueChange) {
						handleValueChange(value);
					}
				}}
				onBlur={() => helpers.setTouched(true)}
				value={field.value}
				error={showError}
				{...rest}
				style={[
					styles.inputStyle,
					{
						backgroundColor: colors.whiteSecondary,
						borderBottomColor: colors.primaryLight,
						fontFamily: primary[300],
					},
				]}
				placeholderTextColor={colors.placeholder}
				secureTextEntry={secureTextEntry}
				cursorColor={colors.textSecondary}
			/>
			{isPasswordField && (
				<Ionicons
					size={30}
					name={secureTextEntry ? "eye-outline" : "eye-off-outline"}
					color={colors.primary}
					style={styles.iconPassword}
					onPress={togglePassword}
				/>
			)}
			{showError && (
				<StyledText variant="secondary" size="sm">
					{meta.error}
				</StyledText>
			)}
		</View>
	);
};

export default FormikTextInput;

const styles = StyleSheet.create({
	inputStyle: {
		position: "relative",
		borderBottomWidth: 1,
		fontSize: 16,
		padding: 16,
	},
	iconPassword: {
		position: "absolute",
		right: 16,
		paddingVertical: 16,
	},
});
