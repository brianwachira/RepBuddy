import React, { useRef, useState, useEffect } from "react";
import { useTheme } from "@react-navigation/native";
import { Pressable, StyleSheet, TextInput, View } from "react-native";
import StyledText from "./StyledText";

type OTPInputProps = {
	code: string;
	setCode: (code: string) => void;
	maximumLength: number;
	setIsPinReady: (isPinReady: boolean) => void;
	isLoading: boolean
};

/**
 * 
 * @param  code: string
 * @param  setCode: (code: string) => void
 * @param maximumLength: number
 * @param  setIsPinReady: (isPinReady: boolean) => void
 * @param isLoading: boolean
 * @returns OTPInput component
 */

const OTPInput = (props: OTPInputProps) => {
	const { colors } = useTheme();

	const { code, setCode, maximumLength, setIsPinReady } = props;

	const boxArray = new Array(maximumLength).fill(0);
	const inputRef = useRef<TextInput>(null);

	const [isInputBoxFocused, setIsInputBoxFocused] = useState(false);

	const handleOnPress = () => {
		setIsInputBoxFocused(true);
		inputRef.current?.focus();
	};

	const handleOnBlur = () => {
		setIsInputBoxFocused(false);
	};

	useEffect(() => {
		// update pin ready status
		setIsPinReady(code.length === maximumLength);
		// clean up function
		return () => {
			setIsPinReady(false);
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [code]);

	const boxDigit = (_: any, index: number) => {
		const emptyInput = "";
		const digit = code[index] || emptyInput;

		const isCurrentValue = index === code.length;
		const isLastValue = index === maximumLength - 1;
		const isCodeComplete = code.length === maximumLength;

		const isValueFocused = isCurrentValue || (isLastValue && isCodeComplete);

		const StyledSplitBoxes =
			isInputBoxFocused && isValueFocused
				? {
						...styles.splitBoxes,
						backgroundColor: colors.background,
						borderBottomColor: colors.primaryDarker,
				  }
				: {
						...styles.splitBoxes,
						backgroundColor: colors.textWhite,
						borderBottomColor: colors.primaryLight,
				  };

		return (
			<View key={index} style={StyledSplitBoxes}>
				<StyledText
					size="md"
					font="light"
					variant="primaryDarker"
					style={styles.splitBoxText}
				>
					{digit}
				</StyledText>
			</View>
		);
	};

	return (
		<View style={styles.otpInputContainer}>
			<Pressable style={styles.splitOtpBoxesContainer} onPress={handleOnPress}>
				{boxArray.map(boxDigit)}
			</Pressable>
			<TextInput
				style={styles.textInputHidden}
				onChangeText={setCode}
				value={code}
				maxLength={maximumLength}
				ref={inputRef}
				onBlur={handleOnBlur}
				editable={props.isLoading}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	otpInputContainer: {
		justifyContent: "center",
		alignItems: "center",
	},
	textInputHidden: {
		position: "absolute",
		opacity: 0,
	},
	splitOtpBoxesContainer: {
		width: "100%",
		flexDirection: "row",
	},
	splitBoxes: {
		borderWidth: 1,
		padding: 16,
		fontSize: 16,
		minWidth: 64,
		marginHorizontal: 12,
		borderTopWidth: 0,
		borderLeftWidth: 0,
		borderRightWidth: 0
	},
	splitBoxText: {
		textAlign: "center",
	},
});
export default OTPInput;
