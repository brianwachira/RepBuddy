import { StyleSheet, TouchableOpacity } from "react-native";
import { useTheme } from "@react-navigation/native";
import StyledText from "./StyledText";
import React, { useState } from "react";
import DateTimePicker, {
	DateTimePickerEvent,
} from "@react-native-community/datetimepicker";
import { formatDate } from "../../util/helpers";
import { MaterialCommunityIcons } from '@expo/vector-icons';

type StyledDatePickerProps = {
	placeholder: string;
	value: Date;
	handleDate: (item: Date | undefined) => void;
	showError?: boolean;
	error?: string
};

/**
 * 
 * @param placeholder: string
 * @param value: Date
 * @param handleDate: (item: Date | undefined) => void
 * @param showError?: boolean
 * @param error?: string 
 * @returns DatePicker component
 */

const StyledDatePicker = ({
	placeholder,
	value,
	handleDate,
	showError,
	error
}: StyledDatePickerProps) => {
	const { colors } = useTheme();
	const [pickerVisible, setPickerVisible] = useState<boolean>(false);

	const onChange = (
		event: DateTimePickerEvent,
		selectedDate: Date | undefined
	) => {
		const currentDate = selectedDate;
		setPickerVisible(!pickerVisible);
		handleDate(currentDate);
	};
	return (
		<>
			<TouchableOpacity
				style={[styles.inputStyle]}
				onPress={() => setPickerVisible(!pickerVisible)}
			>
				{value ? (
					<>
						<StyledText size="sm" font="light" variant="primaryDarker">
							{placeholder}
						</StyledText>
						<StyledText size="md" font="light" variant="textPrimary">
							{formatDate(value)}
						</StyledText>
					</>
				) : (
					<StyledText size="md" font="regular" variant="textSecondary">
						{placeholder}
					</StyledText>
				)}
				<MaterialCommunityIcons name="calendar-month-outline" size={25} color={colors.primaryDarker} style={styles.calendarIcon} />
			</TouchableOpacity>

			{pickerVisible && (
				<DateTimePicker
					testID="dateTimePicker"
					value={value}
					mode={"date"}
					is24Hour={true}
					onChange={onChange}
				/>
			)}
			{showError && (
				<StyledText variant="secondary" size="sm">
					{error}
				</StyledText>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	inputStyle: {
		padding: 16,
	},
	dropDownItemContainer: {
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		borderBottomLeftRadius: 10,
		borderBottomRightRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,

		elevation: 2,
	},
	calendarIcon: {
		position: 'absolute',
		right: 0,
		bottom: 25
	}
});

export default StyledDatePicker;
