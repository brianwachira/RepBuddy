import React from "react";
import { Image } from "expo-image";
import { blurhash } from "../../config/constants";
import { StyleSheet } from "react-native";

const HeaderIcon = () => {
	return (
		// <View style={styles.container}>
			<Image placeholder={blurhash} style={styles.image} source={require("../../../assets/RepBuddy.png")} contentFit="contain" />

		// </View>
	);
};

const styles = StyleSheet.create({
	image: {
		width: '100%',
		height: 36
	},
	container: {
		paddingVertical: 0
	}
})
export default HeaderIcon;
