import React from "react";
import { useTheme } from "@react-navigation/native";
import Row from "../shared/Row";
import StyledText from "../shared/StyledText";
import { Pressable, StyleSheet } from "react-native";
import { Feather } from "@expo/vector-icons";
import Space from "../shared/Space";

interface IHeaderTertiaryProps {
	label: string;
	handlePress: () => void;
}

/**
 * 
 * @param label: string 
 * @param handlePress: () => void
 * @returns HeaderTertiary component
 */

const HeaderTertiary = ({ label, handlePress }: IHeaderTertiaryProps) => {
	const { colors } = useTheme();
	return (
		<Row style={styles.row}>
			<StyledText size="md" font="medium" variant="textSecondary">
				{label}
			</StyledText>
			<Pressable onPress={handlePress} style={styles.buttonRow}>
				<Feather name="plus" size={20} color={colors.primaryDarker} />
				<Space size={4}/> 
				<StyledText font="semiBold" size="base" variant={"primaryDarker"}>
					ADD
				</StyledText>
			</Pressable>
		</Row>
	);
};

const styles = StyleSheet.create({
	row: {
		width: '100%',
	},
	buttonRow: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		alignContent: 'center',
		alignSelf: 'center',
		verticalAlign: 'middle'
	}
})

export default HeaderTertiary;
