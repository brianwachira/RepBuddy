import React from "react";
import { useTheme } from "@react-navigation/native";
import Row from "../shared/Row";
import { Image } from "expo-image";
import { StyleSheet, TouchableOpacity } from "react-native";
import { blurhash } from "../../config/constants";
import { Feather } from "@expo/vector-icons";

type HeaderMainProps = {
	toggleProfile: () => void
}

/**
 * 
 * @param toggleProfile: () => void 
 * @returns HeaderMain component
 */

const HeaderMain = ({toggleProfile}: HeaderMainProps) => {
	const { colors } = useTheme();
	return (
		<Row style={styles.row}>
			<Image
				placeholder={blurhash}
				style={styles.image}
				source={require("../../../assets/RepBuddy.png")}
				contentFit="contain"
			/>

			<TouchableOpacity onPress={toggleProfile}>
				<Feather name="user" size={24} color={colors.primaryDarker} />
			</TouchableOpacity>
		</Row>
	);
};

const styles = StyleSheet.create({
	row: {
		width: '100%',
	},
	image: {
		width: 105,
		height: 30,
	},
});
export default HeaderMain;
