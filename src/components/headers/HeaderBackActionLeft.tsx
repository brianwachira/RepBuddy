import { useTheme } from "@react-navigation/native";
import { Pressable, StyleSheet, View } from "react-native";
import Row from "../shared/Row";
import React from "react";
import StyledText from "../shared/StyledText";
import { Ionicons } from "@expo/vector-icons";
import Space from "../shared/Space";

type HeaderBackActionLeftProps = {
	onPress: () => void;
	label: string;
	column?: boolean;
};

/**
 * 
 * @param onPress: () => void 
 * @param label: string 
 * @param column?: boolean
 * @returns HeaderBackActionLeft
 */

const HeaderBackActionLeft = ({
	onPress,
	label,
	column,
}: HeaderBackActionLeftProps) => {
	const { colors } = useTheme();
	return (
		<>
			{column === true ? (
				<View>
					<Pressable onPress={onPress}>
						<Ionicons
							name="arrow-back-outline"
							size={28}
							color={colors.secondary}
						/>
						<Space size={8} />
						<StyledText variant="textSecondary" size="md" font="medium">
							{label}
						</StyledText>
					</Pressable>
				</View>
			) : (
				<Row>
					<Pressable onPress={onPress} style={styles.itemsCenter}>
						<Ionicons
							name="arrow-back-outline"
							size={28}
							color={colors.secondaryDarker}
						/>
						<Space size={4} horizontal />
						<StyledText variant="secondaryDarker" font="light">
							{label}
						</StyledText>
					</Pressable>
				</Row>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	itemsCenter: {
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row",
	},
});
export default HeaderBackActionLeft;
