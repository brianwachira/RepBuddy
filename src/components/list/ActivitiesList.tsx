import React, { useState } from "react";
import { FlatList, ListRenderItemInfo, StyleSheet, View } from "react-native";
import StyledText from "../shared/StyledText";
import CTA from "../cta";
import Row from "../shared/Row";
import { formatDate } from "../../util/helpers";
import Space from "../shared/Space";
import { IActivity } from "../../../global";
import ActivityCard from "../cards/ActivityCard";
import ActivityModal from "../modals/ActivityModal";

type ActivitiesListProps = {
	item: {
		status: boolean;
		status_message: string;
		activities?: Array<IActivity>
	};
};

/**
 * 
 * @param item: { status: boolean; status_message: string; activities?: Array<IActivity>} 
 * @returns ActivitiesList component
 */

const ActivitiesList = ({ item }: ActivitiesListProps) => {
console.log(item.activities)

const [activity, setActivity] = useState<IActivity | undefined>()

const handleActitvity = (chosenActivity: IActivity) => setActivity(chosenActivity)



	return (
		<>
			{item.status === false ? (
				<View style={styles.container}>
                    <CTA title={"Ready to conquer your day?"} description={"Let this empty task manager be your guide to ultimate productivity and success!"} link={{
                        href: "(dashboard)/addTask",
                        label: "+Add your first task 🎉"
                    }}/>
				</View>
			) : (
				<View style={{flex: 1}}>
					<Row style={{justifyContent: 'flex-start'}}>
						<StyledText size="md" font="medium" variant="textSecondary">Today</StyledText>
						<Space size={4} horizontal/>
						<StyledText size="sm" font="light" variant="textSecondary2">{formatDate(new Date())}</StyledText>
					</Row>
					<FlatList
						data={item?.activities}
						renderItem={({ item }: ListRenderItemInfo<IActivity>) => (
							<ActivityCard
							  onPressCheckBox={() => console.log('pressed')}
							  handlePress={() =>  handleActitvity(item)}
							  item={item}
							/>
						  )}
						  ItemSeparatorComponent={() => <Space size={16}/> }/>
					<ActivityModal
						visible={activity !== undefined}
						item={activity}
						onClose={() =>setActivity(undefined)}
						onPressCheckBox={() => console.log('pressed')}/>
				</View>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 0.89,
		justifyContent: "center",
	},
});
export default ActivitiesList;
