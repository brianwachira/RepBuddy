import { StyleSheet, View } from "react-native";
import StyledText from "../shared/StyledText";
import React from "react";
import { Link } from "expo-router";

type CTAProps = {
    title: string,
    description: string,
    link: {
        href: string,
        label: string
    }
}

/**
 * 
 * @param title: string 
 * @param description: string 
 * @param link: { href: string, label: string} 
 * @returns CTA component
 */

const CTA = ({title, description,link}: CTAProps) => {
    return ( 
        <View style={styles.ctaContainer}>
            <StyledText style={styles.textCenter} font="medium" size="base">
                {title}
            </StyledText>
            <StyledText style={styles.textCenter} font="regular" size="base">
                {description}
            </StyledText>
            <Link href={link.href} style={styles.textCenter}>
                <StyledText size="md" font="light" variant="secondary" underline>
                    {link.label}
                </StyledText>
            </Link>
        </View> );
}

const styles = StyleSheet.create({
	ctaContainer: {
		marginHorizontal: 30,
	},
	textCenter: {
		textAlign: "center",
	},
})
export default CTA