import React, { useState } from "react";
import MainContainer from "../shared/Container";
import Select from "../shared/select/Select";
import Space from "../shared/Space";
import FormikTextInput from "../shared/FormikTextInput";
import { StyleSheet, View } from "react-native";
import StyledButton from "../buttons";
import StyledDatePicker from "../shared/StyledDatePicker";
import { useField } from "formik";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { delay } from "../../util/helpers";
import StyledText from "../shared/StyledText";

interface ITaskFormProps {
	onSubmit: () => void;
	loading: boolean;
	dirty: boolean;
	isValid: boolean;
	handleTaskType: (value: string) => void;
}

/**
 * @param onSubmit: () => void
 * @param loading: boolean
 * @param dirty: boolean
 * @param isValid: boolean
 * @param handleTaskType: (value: string) => void
 * @returns TaskForm component
 */

const TaskForm = ({
	onSubmit,
	loading,
	dirty,
	isValid,
	handleTaskType,
}: ITaskFormProps) => {
	const [field, meta, helpers] = useField<{ label: string; value: string }>(
		"typeOfTask"
	);

	const [location, setLocation] = useState("");
	const handleLocation = (value: string) => setLocation(value);

	const [fieldHcp, metaHcp, helpersHcp] = useField<{
		label: string;
		value: string;
	}>("hcp");
	const [fieldProduct, metaProduct, helpersProduct] = useField<{
		label: string;
		value: string;
	}>("product");

	const [fieldDate, metaDate, helpersDate] = useField<Date>("date");

	const showTaskError: boolean = meta.touched && meta.error ? true : false;
	const showHcpError: boolean = metaHcp.touched && metaHcp.error ? true : false;
	const showProductError: boolean =
		metaProduct.touched && metaProduct.error ? true : false;
	const showDateError: boolean =
		metaDate.touched && metaDate.error ? true : false;

	const { isLoading, error, data } = useQuery({
		queryKey: ["products"],
		queryFn: async () => {
			const response = await axios.get(
				`${process.env.EXPO_PUBLIC_API_URL}/fetch-products`
			);
			return response.data;
		},
	});

	const FetchHcps = async (loc: string) => {
		await delay(2000);
		let response;
		response = await axios.get(
			`${process.env.EXPO_PUBLIC_API_URL}/fetch-clients?location=${loc}`
		);

		return response.data;
	};

	// fetch Hcps based on location change
	const dataHcp = useQuery(["hcps", location], () => FetchHcps(location));

	const renderFields = () => {
		if (field.value.value === "1-on-1") {
			return (
				<>
					<FormikTextInput
						name="location"
						placeholder="Type Location*"
						handleValueChange={handleLocation}
					/>
					<Space size={24} />
					<Select
						placeholder={"Select HCPs*"}
						selectedValue={fieldHcp.value}
						onValueChange={(label: string, value: string) =>
							helpersHcp.setValue({ label, value })
						}
						showError={showHcpError}
						error={metaHcp.error?.label}
					>
						{dataHcp.isLoading ? (
							<View style={styles.loadingContainer}>
								<StyledText>loading...</StyledText>
							</View>
						) : (
							<>
								{dataHcp.data?.data?.map(
									(item: { id: string; name: string }) => (
										<Select.Item
											value={item.id}
											label={item.name}
											key={item.id}
										>
											{item.name}
										</Select.Item>
									)
								)}
							</>
						)}
					</Select>
					{/* Date */}
					<Space size={24} />
					<StyledDatePicker
						value={fieldDate.value}
						placeholder="Set Date*"
						handleDate={(value: Date | undefined) => {
							if (value) {
								helpersDate.setValue(value);
							}
						}}
						showError={showDateError}
						error={metaDate.error}
					/>
					<Space size={24} />
				</>
			);
		} else if (field.value.value === "CME") {
			return (
				<>
					<FormikTextInput
						name="location"
						placeholder="Type Location*"
						handleValueChange={handleLocation}
					/>
					<Space size={24} />
					<Select
						placeholder={"Search Product*"}
						selectedValue={fieldProduct.value}
						onValueChange={(label: string, value: string) =>
							helpersProduct.setValue({ label, value })
						}
						showError={showProductError}
						error={metaProduct.error}
					>
						{isLoading ? (
							<View style={styles.loadingContainer}>
								<StyledText>loading...</StyledText>
							</View>
						) : (
							<>
								{data?.products?.map(
									(item: {
										cost: string;
										created_by: string;
										description: string;
										id: string;
										image_path: string;
										in_stock: string;
										inserted_at: string;
										name: string;
										updated_at: string;
										updated_by: string;
									}) => (
										<Select.Item
											value={item.id}
											label={item.name}
											key={item.id}
										>
											{item.name}
										</Select.Item>
									)
								)}
							</>
						)}
					</Select>
					{/* Date */}
					<Space size={24} />
					<StyledDatePicker
						value={fieldDate.value}
						placeholder="Set Date*"
						handleDate={(value: Date | undefined) => {
							if (value) {
								helpersDate.setValue(value);
							}
						}}
						showError={showDateError}
						error={metaDate.error}
					/>
					<Space size={24} />
					<FormikTextInput name="message" placeholder="Main message" />
					<Space size={24} />
				</>
			);
		} else if (field.value.value === "RTD") {
			return (
				<>
					<FormikTextInput
						name="location"
						placeholder="Type Location*"
						handleValueChange={handleLocation}
					/>
					<Space size={24} />
					<Select
						placeholder={"Select HCPs*"}
						selectedValue={fieldHcp.value}
						onValueChange={(label: string, value: string) =>
							helpersHcp.setValue({ label, value })
						}
						showError={showHcpError}
						error={metaHcp.error?.label}
					>
						{dataHcp.isLoading ? (
							<View style={styles.loadingContainer}>
								<StyledText>loading...</StyledText>
							</View>
						) : (
							<>
								{dataHcp.data?.data?.map(
									(item: { id: string; name: string }) => (
										<Select.Item
											value={item.id}
											label={item.name}
											key={item.id}
										>
											{item.name}
										</Select.Item>
									)
								)}
							</>
						)}
					</Select>
					<Space size={24} />
					<Select
						placeholder={"Search Product*"}
						selectedValue={fieldProduct.value}
						onValueChange={(label: string, value: string) =>
							helpersProduct.setValue({ label, value })
						}
						showError={showProductError}
						error={metaProduct.error?.label}
					>
						{isLoading ? (
							<View style={styles.loadingContainer}>
								<StyledText>loading...</StyledText>
							</View>
						) : (
							<>
								{data?.products?.map(
									(item: {
										cost: string;
										created_by: string;
										description: string;
										id: string;
										image_path: string;
										in_stock: string;
										inserted_at: string;
										name: string;
										updated_at: string;
										updated_by: string;
									}) => (
										<Select.Item
											value={item.id}
											label={item.name}
											key={item.id}
										>
											{item.name}
										</Select.Item>
									)
								)}
							</>
						)}
					</Select>
					{/* Date */}
					<Space size={24} />
					<StyledDatePicker
						value={fieldDate.value}
						placeholder="Set Date*"
						handleDate={(value: Date | undefined) => {
							if (value) {
								helpersDate.setValue(value);
							}
						}}
						showError={showDateError}
						error={metaDate.error}
					/>
					<Space size={24} />
					<FormikTextInput name="message" placeholder="Main message" />
					<Space size={24} />
				</>
			);
		}
	};

	return (
		<View style={styles.formContainer}>
			<MainContainer preset="scroll">
				<Space size={24} />
				<StyledText size="sm" font="light" variant="secondary">*Required</StyledText>
				<Select
					placeholder={"Select type of task*"}
					selectedValue={field.value}
					onValueChange={(label: string, value: string) => {
						helpers.setValue({ label, value });
						handleTaskType(value);
					}}
					showError={showTaskError}
					error={meta.error?.label}
				>
					<Select.Item value={"1-on-1"} label={"One on One (1o1)"}>
						One on One (1o1)
					</Select.Item>
					<Select.Item value={"RTD"} label={"Round Table Discussion (RTD)"}>
						Round Table Discussion (RTD)
					</Select.Item>
					<Select.Item value={"CME"} label={"(CME)"}>
						(CME)
					</Select.Item>
				</Select>
				<Space size={24} />
				{renderFields()}
			</MainContainer>
			<View style={styles.buttonPadding}>
				<StyledButton
					label="CREATE TASK"
					loading={loading || (dirty && !isValid) || field.value.value === ""}
					onPress={onSubmit}
					variant={
						loading || (dirty && !isValid) || field.value.value === ""
							? "disabled"
							: "primary"
					}
				/>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	formContainer: {
		flex: 0.95,
	},
	buttonPadding: {
		paddingHorizontal: 16,
	},
	loadingContainer: {
		padding: 16,
	},
});
export default TaskForm;
