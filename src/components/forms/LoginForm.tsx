import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import StyledText from "../shared/StyledText";
import Space from "../shared/Space";
import FormikTextInput from "../shared/FormikTextInput";
import StyledButton from "../buttons";
import { Link } from "expo-router";

interface ILoginFormProps {
	onSubmit: () => void;
	loading: boolean;
}

/**
 * 
 * @param loading: boolean 
 * @param onSubmit: () => void 
 * @returns LoginForm component
 */

const LoginForm = ({ onSubmit, loading }: ILoginFormProps) => {
	const [showPassword, setShowPassword] = useState(true);

	const togglePassword = () => setShowPassword(!showPassword);

	return (
		<View style={styles.main}>
			<StyledText size="lg" font="light" style={styles.textCenter}>
				Login
			</StyledText>
			<Space size={24} />

			<FormikTextInput
				name="email"
				textContentType="emailAddress"
				autoComplete="email"
				placeholder="Email"
			/>
			<Space size={24} />
			<FormikTextInput
				name="password"
				textContentType="password"
				autoComplete="password"
				togglePassword={togglePassword}
				isPasswordField={true}
				placeholder="password"
				secureTextEntry={showPassword}
			/>
			<Space size={24} />
			<StyledButton label="login" loading={loading} onPress={onSubmit} variant={loading === true ? "disabled" : "primary"} />
			<Space size={16} />
			<Link href={"/forgotpassword"} style={styles.textCenter}>
				<StyledText size="md" font="light" variant="secondary" underline>
					Forgot Password
				</StyledText>
			</Link>
		</View>
	);
};

export default LoginForm;

const styles = StyleSheet.create({
	main: {
		flex: 1,
		justifyContent: "center",
	},
	textCenter: {
		textAlign: "center",
	},
});
