import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import StyledText from "../shared/StyledText";
import Space from "../shared/Space";
import FormikTextInput from "../shared/FormikTextInput";
import StyledButton from "../buttons";
import { Link } from "expo-router";


interface IResetPasswordFormProps {
	onSubmit: () => void;
	loading: boolean;
}

/**
 * 
 * @param onSubmit: () => void 
 * @param loading: boolean
 * @returns ResetPasswordForm 
 */

const ResetPasswordForm = ({ onSubmit, loading}: IResetPasswordFormProps) => {
	const [showPassword, setShowPassword] = useState(true);
	const [showConfirmPassword, setShowConfirmPassword] = useState(true);

	const togglePassword = () => setShowPassword(!showPassword);
	const toggleConfirmPassword = () =>
		setShowConfirmPassword(!showConfirmPassword);

	return (
		<View style={styles.main}>
			<StyledText size="lg" font="light" style={styles.textCenter}>
				Reset Password
			</StyledText>
			<Space size={24} />
			<FormikTextInput
				name="password"
				textContentType="password"
				autoComplete="password"
				placeholder="New Password"
                isPasswordField={true}
                togglePassword={togglePassword}
                secureTextEntry={showPassword}
			/>
			<Space size={24} />
			<FormikTextInput
				name="confirmPassword"
				textContentType="password"
				autoComplete="password"
				placeholder="Confirm Password"
                isPasswordField={true}
                togglePassword={toggleConfirmPassword}
                secureTextEntry={showConfirmPassword}
			/>
            <Space size={24}/>
            <StyledButton label="RESET" loading={loading} onPress={onSubmit}/>
            <Space size={16}/>
            <Link href={'/login'}  style={styles.textCenter}><StyledText size="md" font="light" variant="secondary" underline>Login instead?</StyledText></Link>
		</View>
	);
};

const styles = StyleSheet.create({
	main: {
		flex: 1,
		justifyContent: "center",
	},
	textCenter: {
		textAlign: "center",
	},
});

export default ResetPasswordForm;
