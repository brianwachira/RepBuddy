/**
 * Function to return date in worded format
 * @param dateTimeStr : Date
 * @returns  eg 2023-08-20T09:48:11.255Z returns 20th August 2023
 */
export function formatDate(inputDate: Date): string {
    const date = new Date(inputDate);
    const day = date.getDate();
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();


    const formattedDate = `${getOrdinalSuffix(day)} ${month} ${year}`;
    return formattedDate;
}

/**
 * Function to return ordinal suffix to the day (e.g., 1st, 2nd, 3rd, 4th, etc.)
 * @param n : number
 * @returns 'th', 'st', 'nd', 'rd' eg 1 returns 1st
 */
function getOrdinalSuffix(n: number): string {
    const suffixes = ['th', 'st', 'nd', 'rd'];
    const v = n % 100;
    return n + (suffixes[(v - 20) % 10] || suffixes[v] || suffixes[0]);
}

/**
 * Function to return date in yyyy-mm-dd
 * @param dateTimeStr : Date
 * @returns  eg 2023-08-20T09:48:11.255Z returns 2023-08-26
 */
export function formatDateToYYYYMMDD(dateTimeStr: Date): string {
    const originalDate = new Date(dateTimeStr);

    const year = originalDate.getFullYear();
    const month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
    const day = originalDate.getDate().toString().padStart(2, '0');

    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate;
}



/**
 * Function to return date in delay promise function
 * @param ms : number
 * @returns  Promise<unknown>
 */
export const delay = (ms: number) => new Promise(res => setTimeout(res, ms));