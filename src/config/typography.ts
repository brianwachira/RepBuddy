import type { Fonts, Typography } from "../../global";

export const customFontsToLoad = {
    poppinsRegular: require("../../assets/fonts/Poppins-Regular.ttf"),
    poppinsLight: require("../../assets/fonts/Poppins-Light.ttf"),
    poppinsMedium: require("../../assets/fonts/Poppins-Medium.ttf"),
    poppinsSemiBold: require("../../assets/fonts/Poppins-SemiBold.ttf"),
    poppinsBold: require("../../assets/fonts/Poppins-Bold.ttf"),
}

export const fonts: Fonts = {
    poppins: {
        300: 'poppinsLight',
        400: 'poppinsRegular',
        500: 'poppinsMedium',
        600: 'poppinsSemiBold',
        700: 'poppinsBold',
    }
}

export const typography: Typography = {
    /**
   * The primary font.  Used in most places.
   */
  primary: fonts.poppins
}